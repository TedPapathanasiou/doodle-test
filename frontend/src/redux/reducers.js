import { combineReducers } from 'redux';
import { chatReducers as chat }  from '../chat';
import { loginReducers as login } from '../login';

// The root reducer of the whole application
const rootReducer = combineReducers({
  login,
  chat
});

export default rootReducer;