import React from 'react';
import loadingSpinner from './images/loading-spinner.gif';
import './loadingSpinnerStyles.css';

// LoadingSpinner: Presentational component displaying a loading spinner accompanied by some explanatory text.
const LoadingSpinner = () => {
  return (
    <section className="loading-container">
      <div className="loading-info">
        <div className="help-text">
          <span >Loading, please wait...</span>
        </div>
        <div className="loading-spinner-container">
          <div className="loading-spinner">
            <img src={loadingSpinner} className="loading-icon" alt="loading" width="100" height="100" />
          </div>
        </div>
      </div>
    </section>
  );
}

export default LoadingSpinner;