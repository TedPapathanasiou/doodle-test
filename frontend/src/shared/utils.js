// getDate: Helper function that takes a unix timestamp and returns a date string.
// args: unixTime - integer: A unix timestamp.
function getDate(unixTime) {
  let date = new Date(unixTime * 1000);
  let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  let minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  let hours = date.getHours();
  let day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  let month = months[date.getMonth()];
  let year = date.getFullYear();

  return (`${day} ${month} ${year} ${hours}:${minutes}`);
}

// debounce: Pure js debounce implementation
// args: func - function: The function to be called.
//       wait - integer: The delay of the function call in milliseconds.
//       immediate - boolead: A flag to execute the function immediately.
function debounce(func, wait, immediate) {
  var timeout;
	return function() {
    var context = this, args = arguments;
    console.log(args)
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// fetchWithRetry: A fetch extension which retries to fetch, upon failure, up to n times.
// args: endpoint - string: The api endpoint. 
//       n - integer: The number of maximum retries.
function fetchWithRetry(endpoint, n) {
  return new Promise((resolve, reject) => {
    fetch(endpoint, {
      method: 'GET',
    })
    .then((response) => {
      if(response.ok){
        return resolve(response)
      }
      throw new Error('Network response was not ok.');
    })
    .catch((error) => {
      if(n === 1) {
        return reject(error);
      }
      return resolve(fetchWithRetry(endpoint, n-1));
    })
  });
}

// fetchWithTimeout: A fetchWithRetry extension which throws an error on timeout.
// args: endpoint - string: The api endpoint. 
//       n - integer: The time after which a timeout occures in milliseconds.
function fetchWithTimeout(endpoint, n) {
  let didTimeOut = false;
  return new Promise(function(resolve, reject) {
    const timeout = setTimeout(function() {
      didTimeOut = true;
      return reject(new Error('Request timed out'));
    }, n);
    
    fetchWithRetry(endpoint, 5)
    .then(function(response) {
      clearTimeout(timeout);
      if(!didTimeOut) {
        return resolve(response);
      }
    })
    .catch((err) => {
      if(didTimeOut) return;
      return reject(err);
    });
})
}

export { getDate, debounce, fetchWithRetry, fetchWithTimeout }