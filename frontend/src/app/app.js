import React, { Component } from 'react';
import { hot } from 'react-hot-loader/root';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import rootReducer from '../redux/reducers';
import LoginContainer from '../login';
import ChatContainer from '../chat';

function configureStore(preloadedState) {
  return createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(thunkMiddleware)
  );
}

const store = configureStore();

// App: The root component of the web app.
// It contains the redux and the react-router integration along with the app routs.
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Switch location={location}>
            <Route exact path="/" component={LoginContainer} />
            <Route 
              exact 
              path="/chat" 
              render={() => {
                // If the route is visited organically render the presentational component.
                // Otherwise, redirect to the landing page.
                if(store.getState().login.username !== '') {
                  return (<ChatContainer />);}
                else {
                  return (<Redirect to='/' />);}
              }}
            />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default hot(App);
