import React, { Component } from 'react';

// InputArea: The presentational component containing the textarea and send button of the UI.
export default class InputArea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleSend = this.handleSend.bind(this);
  }

  // Handler for the controlled textarea element.
  handleTextChange(event) {
    let target = event.target;
    this.setState({ text: target.value });
  }

  // Handler for the send button. Dispatches an action to send the message to the server
  handleSend(event) {
    this.props.sendMessage(this.state.text, this.props.socket);
    this.setState({text: ''});
    event.preventDefault();
  }

  render() {
    return (
      <div className="input-container">
        <textarea type="text" className="input-text" rows="3" value={this.state.text} onChange={this.handleTextChange} />
        <button type="submit" className="input-button" onClick={this.handleSend}>Send</button>
      </div>
    );
  }
}