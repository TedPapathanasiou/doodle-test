import React, { Component } from 'react';
import { connect } from 'react-redux';
import io from 'socket.io-client';
import * as actions from './chatActions';
import MessagesArea from './chatMessagesArea';
import InputArea from './chatInputArea';
import './chatStyles.css';

// Chat: The container component of the chat feature. It contains the MessagesArea and InputAre presentational
// components. Chat receives its props from the store and establishes the socket event listeners in its 
// componentDidMount lifecycle function.
export class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // The socket connecting to the server.
      socket: io.connect('http://localhost:5000', {forceNew: true})
    }
  }

  componentDidMount() {
    // Upon connecting to the server, Chat fetches the saved messages and listens for to new ones.
    this.state.socket.on('connect', () => {
      this.props.initialConnection(this.state.socket);
      this.props.listenForMessages(this.state.socket);
    })
  }

  render() {
    // Splitting of the props so they can be passed easily to the children components.
    let { initialConnection, listenForMessages, sendMessage, ...otherProps } = this.props;
    
    return (
      <div className="container">
        <MessagesArea {...otherProps} />
        <InputArea sendMessage={sendMessage} socket={this.state.socket} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
  messages: state.chat.messages,
  loaded: state.chat.loaded
})};

const mapDispatchToProps = (dispatch) => ({
  initialConnection: (socket) => dispatch(actions.initialConnection(socket)),
  listenForMessages: (socket) => dispatch(actions.listenForMessages(socket)),
  sendMessage: (msg, socket) => dispatch(actions.sendMessage(msg, socket)),
});

const ChatContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);

export default ChatContainer;