import React from 'react';
import { shallow, mount } from 'enzyme';

import io, {serverSocket, cleanUp } from 'socket.io-client';
import { Chat } from './chat';
import MessagesArea from './chatMessagesArea';
import InputArea from './chatInputArea';
import LoadingSpinner from '../shared/loadingSpinner';



describe('Chat', function() {
  afterEach(cleanUp);

  let props = {
    loaded: false,
    messages: [],
    initialConnection: jest.fn(),
    listenForMessages: jest.fn(),
    sendMessage: jest.fn()
  }

  
  // RENDER TESTS
  it('should render correctly with the minimum props', () => {
    const component = shallow(<Chat {...props} />);

    expect(component).toMatchSnapshot();
  });

  it('should contain a Messages and an Input are', () => {
    const component = shallow(<Chat {...props} />);

    expect(component.find(MessagesArea)).toHaveLength(1);
    expect(component.find(InputArea)).toHaveLength(1);
  });

  // FUNCTIONALITY TESTS
  it('should call initialConnection and listenForMessages on mounting', () => {
    const component = shallow(<Chat {...props} />);
    serverSocket.emit('connect');

    expect(props.initialConnection).toHaveBeenCalled();
    expect(props.listenForMessages).toHaveBeenCalled();
  });
});




describe('MessagesAre', function() {
  Element.prototype.scrollIntoView = () => {};
  
  let props = {
    messages: [{
      id: 0,
      user: 'user',
      text: 'text',
      date: 0
    }],
    loaded: false
  }

  // RENDER TESTS
  it('should render correctly with the minimum props', () => {
    const component = mount(<MessagesArea {...props} />);

    expect(component).toMatchSnapshot();
  });

  it('should render correctly when in loading state', () => {
    props.loaded = false;
    const component = mount(<MessagesArea loaded={false} {...props} />);
    props.loaded = true;

    expect(component.find(LoadingSpinner)).toHaveLength(1);
  });

  it('should call dataToUI and render the messages', () => {
    const component = mount(<MessagesArea {...props} />);
    const instance = component.instance();
    jest.spyOn(instance, 'dataToUI');
    instance.forceUpdate();

    expect(instance.dataToUI).toHaveBeenCalled();
    expect(component.find('.message')).toHaveLength(1);
  });

  // FUNCTIONALITY TESTS
  it('should call scrollToBottom on mount', () => {
    const component = mount(<MessagesArea {...props} />);
    const instance = component.instance();
    jest.spyOn(instance, 'scrollToBottom');
    instance.forceUpdate();
    
    expect(instance.scrollToBottom).toHaveBeenCalled();
  });
});




describe('InputArea', function() {
  let props = {
    sendMessage: jest.fn()
  }

  // RENDER TESTS
  it('should render correctly with the minimum props', () => {
    const component = shallow(<InputArea {...props} />);

    expect(component).toMatchSnapshot();
  });

  // FUNCTIONALITY TESTS
  it('should call sendMessage on a send button click', () => {
    const component = shallow(<InputArea {...props} />);
    component
      .find('.input-button')
      .simulate('click', {preventDefault: jest.fn()});
  
    expect(props.sendMessage).toHaveBeenCalled();
  });
  
  it('should call handleTextChange on input\'s value change', () => {
    const component = shallow(<InputArea {...props} />);
    const instance = component.instance();
    jest.spyOn(instance, 'handleTextChange');
    instance.forceUpdate();
    component
      .find('.input-text')
      .simulate('change', {target: {value: "a"}});
  
    expect(instance.handleTextChange).toHaveBeenCalled();
  });
});