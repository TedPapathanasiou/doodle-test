import React, { Component } from 'react';
import { getDate } from '../shared/utils';
import LoadingSpinner from '../shared/loadingSpinner';

// MessagesArea: The presentational component displaying all the messages.
class MessagesArea extends Component {
  constructor(props) {
    super(props);

    this.dataToUI = this.dataToUI.bind(this);
    this.scrollToBottom = this.scrollToBottom.bind(this);

    this.scrollRef = React.createRef();
  }

  // dataToUI: Maps the messages to UI.
  // args: messages - array: An array of message objects
  dataToUI(messages) {
    return messages.map((msg) => {
      // Case where the message is sent my the current user
      if(msg.user === '') {
        return (
          <div key={msg.id} className="message me">
            <div className="message-user">
              You
            </div>
            <div className="message-text">
              <pre>
                {msg.text}
              </pre>
            </div>
            <div className="message-date">
              {getDate(msg.date)}
            </div>
          </div>
        );
      }
      // Case where the message is from a different user.
      else {
        return (
          <div key={msg.id} className="message">
            <div className="message-user">
              {msg.username}
            </div>
            <div className="message-text">
              <pre>
                {msg.text}
              </pre>
            </div>
            <div className="message-date">
              {getDate(msg.date)}
            </div>
          </div>
        );
      }
    });
  }

  // Helper function to scroll MessagesArea to the bottom
  scrollToBottom() {
    this.scrollRef.current.scrollIntoView({ behaviour: 'smooth' });
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    // Case where the messages are loaded
    if(this.props.loaded){
      return (
        <div className="messages-container">
          {this.dataToUI(this.props.messages)}
          <div ref={this.scrollRef}></div>
        </div>
      );
    }
    // Case were the messages are still loading.
    // Displays a loading spinner
    else {
      return (
        <div className="messages-container">
          <LoadingSpinner />
          <div ref={this.scrollRef}></div>
        </div>
      );
    }
    
  }
}

export default MessagesArea;