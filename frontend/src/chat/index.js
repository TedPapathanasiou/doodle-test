// Import all relevant files of the feature and export them through one place.
import * as chatTypes from './chatTypes';
export { chatTypes };

import * as chatActions from './chatActions';
export { chatActions };

import chatReducers from './chatReducers';
export { chatReducers };

import ChatContainer from './chat';
export default ChatContainer;