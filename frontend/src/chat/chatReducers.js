import * as types from './chatTypes';

// The initial state of the chat feature
const initialState = {
  loaded: false,
  messages: []
}

// The Chat feature reducer.
const chatReducers = (state = initialState, action) => {
  switch(action.type) {
    case types.REQUEST_MESSAGES:
      return {
        ...state,
        loaded: false
      };
    case types.RECEIVE_MESSAGES:
      let obj = JSON.parse(action.messages);
      return { 
        ...state, 
        loaded: true, 
        messages: [...state.messages, ...obj.messages]
      };
    default:
      return state;
  }
}

export default chatReducers;