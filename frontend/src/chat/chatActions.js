import * as types from './chatTypes';

// Basic actions. Sent to the reducers
function requestMessages() {
  return ({
    type: types.REQUEST_MESSAGES
  });
}

function receiveMessages(messages) {
  return ({
    type: types.RECEIVE_MESSAGES,
    messages
  });
}

// Complex actions responsible for API calls. Not sent directly to the reducers

// Emits the INITIAL_CONNECTION event to server in order to receive all saved messages
function initialConnection(socket) {
  return (dispatch) => {
    dispatch(requestMessages());
    socket.emit('INITIAL_CONNECTION');
  }
}

// Listens for the event OUTBOUND_MESSAGE from the server. Then sends received messages to the reducers
function listenForMessages(socket) {
  return (dispatch) => {
    socket.on('OUTBOUND_MESSAGE', (messages) => dispatch(receiveMessages(messages)));
  }
}

// Emits the INBOUND_MESSAGE event to the server. It also appends the sent message localy without waiting for the server.
// This functionality can be expanded to immitate modern messaging apps
function sendMessage(text, socket) {
  return (dispatch, getState) => {
    const user = getState().login.username;
    let msg = {
      user: user,
      text: text
    }
    socket.emit('INBOUND_MESSAGE', JSON.stringify(msg));

    // Complete the message object and send it to the reducers.
    msg.user = '';
    msg.date = Math.round((new Date()).getTime() / 1000);
    msg.id = new Date().getTime();
    
    dispatch(receiveMessages(JSON.stringify({messages: [msg]})));
  }
}

export { initialConnection, listenForMessages, sendMessage };