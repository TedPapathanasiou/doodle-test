// Import all relevant files of the feature and export them through one place.
import * as loginTypes from './loginTypes';
export { loginTypes };

import * as loginActions from './loginActions';
export { loginActions };

import loginReducers from './loginReducers';
export { loginReducers };

import LoginContainer from './login';
export default LoginContainer;