import * as types from './loginTypes';

// The initial state of the login feature
const initialState = {
  username: ''
}

// The Login feature reducer.
const loginReducers = (state = initialState, action) => {
  switch(action.type) {
    case types.LOGIN:
      return {
        ...state,
        username: action.username
      };
    default:
      return state;
  }
}

export default loginReducers;