import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from './loginActions';
import './loginStyles.css';

// Login: Presentational component displaying the username input and enter button.
export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: ''
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
  }

  // Handler for the controlled input element.
  handleTextChange(event) {
    let target = event.target;
    this.setState({ username: target.value });
  }

  // Handler for the enter button. On click it sends an action to the store and navigates 
  // to the chat by pushing the chat route to the browser's history.
  handleEnter(event) {
    if(this.state.username !== '') {
      this.props.login(this.state.username);
      this.setState({text: ''});
      this.props.history.push('/chat');
    }
    event.preventDefault();
  }

  render() {
    return (
      <div className="login-container">
        <label>ENTER YOUR NAME:</label>
        <input type="text" className="login-text" value={this.state.text} onChange={this.handleTextChange} />
        <button type="submit" className="login-button" onClick={this.handleEnter}>Enter</button>
      </div>
    );
  }
}

// Loggin wrapped to receive the history prop.
export const LoginWithRouter = withRouter(Login);

const mapDispatchToProps = (dispatch) => ({
  login: (user) => dispatch(actions.login(user)),
});

// The container component
const LoginContainer = connect(
  null,
  mapDispatchToProps
)(LoginWithRouter);

export default LoginContainer;