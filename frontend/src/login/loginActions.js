import * as types from './loginTypes';

// Basic actions. Sent to the reducers
export function login(user) {
  return ({
    type: types.LOGIN,
    username: user
  });
}