# Doodle technical test
A basic public chat app

## Getting Started

Follow the steps in "Installation and Running the Project" to begin.

### Installation and Running the Project

Download or clone the repository to your machine.

Open a terminal in the frontend folder.

* Run npm install

To run the server open a terminal in the api-server folder

* Run . env/bin/activate
* Run ./app.py

The server should begin in 127.0.0.1:5000.

To run the frontend open another terminal in the frontend folder

* Run npm start

The webpack-dev-server should begin in 127.0.0.1:3000

You can also build the frontend and deploy the dist folder through a server of your choice.

To run the frontend tests open a terminal in the frontend folder

* Run npm run test

Jest should run all the tests and display the results.

## Built With

* [React](https://reactjs.org/) - A JavaScript library for building user interfaces.
* [Redux](https://redux.js.org/) - A predictable state container for JavaScript apps.
* [Flask](http://flask.pocoo.org/) - Flask, A Python Microframework.
* [Socket.io](http://flask.pocoo.org/) - A JavaScript library for realtime web applications.

## Authors

* [**Ted Pap**](https://github.com/TedPap)
