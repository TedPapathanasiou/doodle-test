import psycopg2
import time
from dbConf import *

# Class DBF provides all the queries to the database used by the server API
class DBF():
  conn = psycopg2.connect(host=DB_HOST, database=DB_NAME, user=DB_USER, password=DB_PASSWORD)
    
  # Inserts into db table messages a message with all the appropriate fields
  def insertMessage(self, msg):
    try:
      cur = self.conn.cursor()
      date = int(time.time())
      sql = 'INSERT INTO public.messages (username,text,date) VALUES (%s,%s,%s);'
      values = (msg["user"], msg["text"], date)
      cur.execute(sql, values)
      self.conn.commit()
      cur.close()
    except(Exception, psycopg2.DatabaseError) as error:
      print(error)
    finally:
      return [{ "username": msg["user"], "text": msg["text"], "date": date }]
  
  # Fetches all messages from the DB table messages
  def readMessages(self):
    # conn = None
    res = []
    try:
      cur = self.conn.cursor()
      sql = 'SELECT * FROM public.messages LIMIT 100'
      cur.execute(sql)
      res = cur.fetchall()
      resList = []
      for row in res:
        resList.append({
          "id" : row[0],
          "username" : row[1],
          "text" : row[2],
          "date" : row[3]
        })
      cur.close()
    except(Exception, psycopg2.DatabaseError) as error:
      print(error)
      return res
    finally:
      return resList
