#!/usr/bin/env python
from flask import Flask, request
from flask_cors import CORS
from flask_socketio import SocketIO
import json
import time

import eventTypes
from dbFuncs import DBF


app = Flask(__name__)
app.config["SECRET_KEY"] = "some_hash_or_key"
socketio = SocketIO(app)
CORS(app)

dbf = DBF();

# Listener for the INITIAL_CONNECTION event. Upon firing it fetches all messages from the DB
# and sends them back to the user that fired the event.
@socketio.on(eventTypes.INITIAL_CONNECTION)
def handle_initial_connection(methods=["GET","POST"]):
  data = dbf.readMessages()
  dataJSON = json.dumps({"messages": data})
  socketio.emit(eventTypes.OUTBOUND_MESSAGE, dataJSON, room=request.sid)

# Listener for the INBOUND_MESSAGE event. Upon firing it stores the message to the database and
# sends it to all other connected clients.
@socketio.on(eventTypes.INBOUND_MESSAGE)
def handle_inbound_message(msg, methods=["GET", "POST"]):
  messages = dbf.insertMessage(json.loads(msg))
  socketio.emit(eventTypes.OUTBOUND_MESSAGE, json.dumps({"messages": messages}), include_self=False)

if __name__ == "__main__":
  socketio.run(app, port=5000, debug=True)